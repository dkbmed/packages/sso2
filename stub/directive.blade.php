<div id="sso"></div>
<script async>!function () {
  window.sso_config = %s;
  window.sso_signed = '{!! json_encode(app('SSODataContainer')->user) !!}';
  var d = document.createElement("script");
  d.src = "%s/build/sso.js";
  d.type = "module";
  document.body.appendChild(d);
}();</script>
<link rel="stylesheet" href="%s/build/sso.css">

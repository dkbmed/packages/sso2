<?php

return [
    'api' => env('SSO_AUTH_HOST', 'https://sso.dkbmed.com'),
    'assets' => env('SSO_MODAL_HOST', 'https://sso.dkbmed.com'),

    'user_model' => '\App\Models\User',
    'column' => 'sso_id',

//  WARNING: this options will be public visible, so no tokens\pass\etc here!
    'public' => [
        'api' => env('SSO_EXTERNAL_AUTH_HOST', env('SSO_AUTH_HOST', 'https://sso.dkbmed.com')),
        'banner' => env('SSO_MODAL_HOST') . '/assets/banner.jpg',
        'additionalDataSource' => env('SSO_ADDITIONAL_DATA_DB'),

        'mailchimp' => [
            'subscribe' => true,
            'tags' => ['covid19']
        ],

        'additional' => [
            [
                'type' => 'text',
                'name' => 'test',
                'label' => 'Example:<br>1 will invoke sub field,<br>2 will make sub field required<br>and this field is required',
                'required' => true,
                'placeholder' => 'sure',
                'validation' => 'numeric', // validation should be Laravel compatible
                'sub_fields' => [
                    [
                        'type' => 'text',
                        'name' => 'number',
                        'invoke_value' => ['1', '2'], // should be sting or array of strings, no matter what
                        'label' => 'Numeric value',
                        'placeholder' => 'random',
                        'validation' => 'numeric|nullable|required_if:test,2', // even if type is numeric, it should also mentioned in validation
                    ]
                ]
            ], [
                'label' => 'Country',
                'name' => 'country_id',
                'placeholder' => 'Please select',
                'type' => 'select',
                'required' => true,
                'options' => [
                    [
                        'label' => 'US',
                        'value' => '1',
                    ], [
                        'label' => 'Ukraine',
                        'value' => '2',
                    ], [
                        'label' => 'Other',
                        'value' => '3',
                    ],
                ],
                'sub_fields' => [
                    [
                        'type' => 'select',
                        'label' => 'State',
                        'name' => 'state_id',
                        'invoke_value' => '1',
                        'placeholder' => 'State',
                        'validation' => 'required_if:country_id,1',
                        'options' => [
                            [
                                'label' => 'New York',
                                'value' => '1',
                            ], [
                                'label' => 'Alabama',
                                'value' => '2',
                            ], [
                                'label' => 'Other',
                                'value' => '3',
                            ],
                        ],
                    ]
                ]
            ], [
                'label' => 'ABIM',
                'type' => 'text',
                'name' => 'abim',
                'required' => true,
                'placeholder' => 'ABIM number in main DB',
            ]
        ],
    ],
];

<?php

namespace DKBmed\SSO\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

trait SSOUser
{
    use SSOUserHelpers;

    private $ssoUser;
    private static $guzzle;
    private static $host;

    public static function bootSSOUser()
    {
        self::$host = config('sso.api');
        self::$guzzle = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
                'Authorization' => 'Bearer ' . request()->cookie('dkb_token')
            ]
        ]);

        static::retrieved(function ($user) {
            $user->populate();
        });
    }

    public function __get($key)
    {
        if (
            $key !== 'id'
            && $this->ssoUser
            && property_exists($this->ssoUser, $key)
            && !$this->hasAttribute($key)
        ) {
            return $this->ssoUser->{$key};
        }

        return parent::__get($key);
    }

    public function populate()
    {
        $ssoColumn = Config::get('sso.column', 'sso_id');
        $ssoUser = App::make('SSODataContainer')->user;
        $ssoId = $this->{$ssoColumn};

        if ($ssoUser && $ssoId === $ssoUser->sub) {
            $this->ssoUser = $ssoUser;
        } else {
            $this->ssoUser = Cache::remember('sso.tenant.user.' . $ssoId, 1800, function () use ($ssoId) {
                return json_decode(
                    (string)self::$guzzle
                        ->request('GET', self::$host . '/api/tenant/user/' . $ssoId)
                        ->getBody()
                );
            });
        }

        return $this;
    }

    public function updateOrCreateSSO($attributes)
    {
        if (isset($attributes['image'])) {
            $attributes['image'] = base64_encode(file_get_contents($attributes['image']));
        }

        try {
            $response = self::$guzzle->request(
                'PATCH',
                self::$host . '/api/user',
                ['json' => $attributes]
            );
        } catch (GuzzleException $e) {
            Log::debug($e);
            return false;
        }

        $data = json_decode((string)$response->getBody());
        $this->ssoUser = $data->user;

        $this->setTokenCookie($data->token);

        return $data;
    }

    public function refreshToken()
    {
        $response = self::$guzzle->request('GET', self::$host . '/api/auth/refresh');
        $data = json_decode((string)$response->getBody());
        $this->ssoUser = $data->user;

        $this->setTokenCookie($data->token);
    }

    public function isCustomDataRequired()
    {
        foreach (config('sso.public.additional') as $key => $field) {
            if (
                ($field['required'] ?? false)
                && (Schema::hasColumn($this->getTable(), $key) || ($field['main'] ?? false))
                && $this->$key === null
            ) {
                return true;
            }
        }
        return false;
    }

    public function customData()
    {
        $model = $this;
        $fields = config('sso.public.additional_fields');
        $result = collect([]);

        array_walk($fields, function ($field, $key) use (&$result, $model) {
            $result[$key] = $model->$key;
            if ($field['extra'] ?? false) {
                $extraKey = $field['extra']['key'];
                $result[$extraKey] = $model->{$field['extra']['key']};
            }
        });

        return $result;
    }

    private function setTokenCookie($token)
    {
        Cookie::queue(cookie('dkb_token')
            ->withExpires(now()->addYear())
            ->withValue($token)
            ->withHttpOnly(false)
            ->withSameSite('None')
            ->withPath('/')
            ->withDomain(env('SSO_COOKIE_DOMAIN', '.dkbmed.com')));
    }

    private function hasAttribute($attr)
    {
        return array_key_exists($attr, $this->attributes);
    }
}

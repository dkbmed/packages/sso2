<?php

namespace DKBmed\SSO\Traits;

trait SSOUserHelpers
{
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getProfessionNameAttribute()
    {
        $professionTitle = $this->profession_title;

        return $this->full_name .
            (($professionTitle != 'Other' && $professionTitle != 'N/A') ? ', ' . $professionTitle : '');
    }

    public function getTitleNameAttribute()
    {
        return $this->full_name . ', ' . $this->title;
    }
}

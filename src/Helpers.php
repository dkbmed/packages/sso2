<?php

namespace DKBmed\SSO;

class Helpers
{
    public static function toObj($array)
    {
        return json_decode(json_encode($array));
    }

    public static function encodeSSOParams($array)
    {
        return urlencode(json_encode($array));
    }

    public static function decodeJWT($token)
    {
        if (!$token) {
            return null;
        }

        return json_decode(base64_decode(
            str_replace('_', '/', str_replace('-', '+', explode('.', $token)[1]))
        ));
    }
}

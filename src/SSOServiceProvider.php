<?php

namespace DKBmed\SSO;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Cookie\Middleware\EncryptCookies;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $config = Helpers::toObj(config('sso'));

        $this->app->resolving(EncryptCookies::class, function ($object) {
            $object->disableFor(['dkb_token']);
        });

        $this->app->singleton('SSODataContainer', function () {
            return new class{
                public $user;
            };
        });

        $kernel = $this->app->make(\Illuminate\Contracts\Http\Kernel::class);
        $kernel->appendMiddlewareToGroup('web', \DKBmed\SSO\Middleware\DKBToken::class);
        $kernel->prependMiddlewareToGroup('api', \DKBmed\SSO\Middleware\DKBToken::class);

        $this->loadViewsFrom(__DIR__ . '/templates', 'sso');

        $this->publishes([
            __DIR__ . '/../config/sso.php' => config_path('sso.php')
        ], 'config');


        $this->loadBladeDirective($config);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/sso.php', 'sso'
        );
    }

    protected function loadBladeDirective($config)
    {
        $publicConfig = json_encode($config->public);

        $stub = file_get_contents(__DIR__ . '/../stub/directive.blade.php');
        $stub = sprintf($stub, $publicConfig, $config->assets, $config->assets);

        Blade::directive('injectDKBmedSSO', function () use ($stub) {
            return $stub;
        });
    }
}

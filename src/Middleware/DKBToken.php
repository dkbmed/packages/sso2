<?php

namespace DKBmed\SSO\Middleware;

use Closure;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class DKBToken
{
    public function handle($request, Closure $next)
    {
        $host = config('sso.api');

        $isApi = $request->wantsJson();
        $token = $isApi ? $request->bearerToken() : $request->cookie('dkb_token');

        if ($token) {
            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request(
                    'GET',
                    $host . '/api/auth/me',
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Authorization' => 'Bearer ' . $token
                        ]
                    ]);
            } catch (GuzzleException $e) {
                if ($isApi) {
                    return abort('401', 'Token expired or invalid');
                }

                return redirect(config('sso.public.redirect', '/'))->withCookie(
                    cookie()->make('dkb_token', null, -2628000, '/',
                        env('SSO_COOKIE_DOMAIN', '.dkbmed.com'),
                        null, false, false,
                        'None'
                    )
                );
            }


            $ssoUser = json_decode($response->getBody());
            App::make('SSODataContainer')->user = $ssoUser;
            $ssoIDColumn = config('sso.column', 'sso_id');

            if (
                !Auth::check()
                || ($ssoUser->id ?? $ssoUser->sub) !== Auth::user()->$ssoIDColumn
            ) {
                $user = config('sso.user_model')::firstOrCreate(
                    [$ssoIDColumn => $ssoUser->id ?? $ssoUser->sub],
                    [$ssoIDColumn => $ssoUser->id ?? $ssoUser->sub, 'role' => 'user']
                );
                $user->populate();

                Auth::login($user);
            }
        } else {
            Auth::logout();
        }

        return $next($request);
    }
}

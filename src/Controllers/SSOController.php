<?php

namespace DKBmed\SSO\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;

class SSOController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }

    // TODO: deprecated
    public function storeAdditionalData(Request $request)
    {
        $validated = $request->validate(
            array_reduce(config('sso.public.additional'), function ($acc, $field) {
                foreach ($field['sub_fields'] ?? [] as $subField) {
                    $acc = array_merge($acc, $this->makeValidationRule($subField, $field['name']));
                }

                return array_merge($acc, $this->makeValidationRule($field));
            }, [])
        );

        $user = auth()->user();
        $user->unguard();

        // TODO: refactor this
        return false;

//        dd($request->get('main.*'));
        $user->update($validated);

        $keys = [];
        $mainKeys = [];
        foreach (config('sso.public.additional') as $key => $field) {
            if (
                ($field['extra']['invoke_value'] ?? false)
                && \request()->has($key)
                && (\request()->get($key) === $field['extra']['invoke_value'])
                && ($field['extra'] ?? false)
            ) {
                if ($field['extra']['main'] ?? false) {
                    $mainKeys[] = $field['extra']['key'];
                } else {
                    $keys[] = $field['extra']['key'];
                    continue;
                }
            }

            if ($field['main'] ?? false) {
                $mainKeys[] = $key;
                continue;
            }

            $keys[] = $key;
        }

        $user->update($request->only($keys));
        $user->updateOrCreateSSO([
                'email' => $user->email,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
            ] + $request->only($mainKeys));
    }

    private function makeValidationRule($field, $parentName = null)
    {
        $validation = $field['required'] ?? false ? 'required|' : '';

        if (
            $parentName
            && $field['invoke_value']
            && ($field['required'] ?? false)
        ) {
            $validation = 'required_if:' . $parentName . ',' . $field['invoke_value'] . '|';
        }

        $validation .= $field['validation'] ?? '';

        return [$field['name'] => rtrim($validation, '|')];
    }
}
